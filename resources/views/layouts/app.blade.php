<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Lukas Kämmerling</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Load Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&subset=cyrillic" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="css/app.css"/>
    >

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicons/favicon.ico">

</head>

<body>

<!-- Page -->
<div class="page" id="home-section">


    <!-- Started Background -->
    <div class="started-bg">
        <div class="slide" style="background-image: url('images/particles-bg.jpg');"></div>
    </div>

    <!-- Header -->
    <header>
        <div class="top-menu">
            <ul>
                <li>
                    <a class="btn_animated" href="#about-section"><span class="circle">Über mich</span></a>
                </li>
                <!-- <li>
                     <a class="btn_animated" href="#skills-section"><span class="circle">Skills</span></a>
                 </li>-->
                <li>
                    <a class="btn_animated" href="#experience-section"><span class="circle">Arbeitsstationen</span></a>
                </li>
                <li>
                    <a class="btn_animated" href="#education-section"><span class="circle">Schulbildung</span></a>
                </li>
                <li>
                    <a class="btn_animated" href="#projects-section"><span class="circle">Projekte und Angebote</span></a>
                </li>
                <li>
                    <a class="btn_animated" href="#contact-section"><span class="circle">Kontakt</span></a>
                </li>
            </ul>
            <a href="#" class="menu-btn"><span></span></a>
        </div>
    </header>

    <!-- Container -->
    <div class="container">

        <!-- Wrapper -->
        <div class="wrapper">
            <!-- Started -->
        <!--<div class="section started">
                <div class="st-box">
                    <div class="st-bts">
                        <a href="mailto:{{ trans('frontpage.mailaddress') }}" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-envelope-o"></i></span>
                        </a>
                    </div>
                    <div class="st-image"><img src="images/man.jpg" alt=""/></div>
                    <div class="st-title">Lukas Kämmerling</div>
                    <div class="st-subtitle">Full-Stack Webdeveloper | Fachinformatiker Anwendungsentwicklung</div>
                    <div class="st-soc">
                        <a target="blank" href="https://github.com/lkdevelopment" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-github"></i></span>
                        </a>
                        <a target="blank" href="https://bitbucket.org/LukasKae" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-bitbucket"></i></span>
                        </a>
                        <a target="blank" href="https://instagram.com/" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-instagram"></i></span>
                        </a>
                        <a target="blank" href="https://twitter.com/LukasKae" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-twitter"></i></span>
                        </a>
                        <a target="blank" href="https://www.xing.com/profile/Lukas_Kaemmerling2" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-xing"></i></span>
                        </a>
                        <a href="skype:lukasakagmac" class="btn_animated">
                            <span class="circle"><i class="icon fa fa-skype"></i></span>
                        </a>
                    </div>
                </div>
            </div>-->

            <div class="section about started" id="about-section">
                <div class="content-box">
                    <div class="row">
                        <div class="col col-m-12 col-t-5 col-d-5">
                            <div class="st-box mobile">
                                <div class="st-title">Lukas Kämmerling</div>
                                <div class="st-subtitle">Full-Stack Webdeveloper | Fachinformatiker
                                    Anwendungsentwicklung
                                </div>
                                <div class="st-soc">
                                    <a target="blank" href="https://github.com/lkdevelopment" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-github"></i></span>
                                    </a>
                                    <a target="blank" href="https://bitbucket.org/LukasKae" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-bitbucket"></i></span>
                                    </a>
                                    <a target="blank" href="https://instagram.com/" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-instagram"></i></span>
                                    </a>
                                    <a target="blank" href="https://twitter.com/LukasKae" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-twitter"></i></span>
                                    </a>
                                    <a target="blank" href="https://www.xing.com/profile/Lukas_Kaemmerling2" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-xing"></i></span>
                                    </a>
                                    <a href="skype:lukasakagmac" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-skype"></i></span>
                                    </a>
                                </div>
                            </div>
                            <div class="profile-image">
                                <img src="images/man.jpg" alt="">
                            </div>
                            <div class="info-list">
                                <ul>
                                    <li><strong><span>{{ trans('frontpage.age') }}
                                                :</span></strong> {{ date('Y') - 1997 }} {{ trans('frontpage.years') }}
                                        (13.08.1997)
                                    </li>
                                    <li><strong><span>{{ trans('frontpage.job') }}
                                                :</span></strong> {{ trans('frontpage.jobtitle') }}</li>
                                    <li><strong><span>{{ trans('frontpage.mail') }}:</span></strong>
                                        <a href="mailto:{{ trans('frontpage.mailaddress') }}">{{ trans('frontpage.mailaddress') }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col col-m-12 col-t-7 col-d-7">
                            <div class="st-box desctop">
                                <div class="st-title">Lukas Kämmerling</div>
                                <div class="st-subtitle">Full-Stack Webdeveloper | Fachinformatiker
                                    Anwendungsentwicklung
                                </div>
                                <div class="st-soc">
                                    <a target="blank" href="https://github.com/lkdevelopment" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-github"></i></span>
                                    </a>
                                    <a target="blank" href="https://bitbucket.org/LukasKae" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-bitbucket"></i></span>
                                    </a>
                                    <a target="blank" href="https://instagram.com/" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-instagram"></i></span>
                                    </a>
                                    <a target="blank" href="https://twitter.com/LukasKae" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-twitter"></i></span>
                                    </a>
                                    <a target="blank" href="https://www.xing.com/profile/Lukas_Kaemmerling2" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-xing"></i></span>
                                    </a>
                                    <a href="skype:lukasakagmac" class="btn_animated">
                                        <span class="circle"><i class="icon fa fa-skype"></i></span>
                                    </a>
                                </div>
                            </div>
                            <div class="text-box">
                                <p>
                                    Lukas, geboren am 13.08.1997 in Neuss, Sohn eines promovierten Chemikers und einer
                                    Sozialpädagogin, entdeckte schon früh die Leidenschaft für Computer. Im Alter von 9
                                    Jahren bekam er zu seiner Kommunion einen Computer geschenkt, welcher zuerst nur zum
                                    Spielen von Computerspielen gebraucht wurde, sich später aber zu seinem
                                    Arbeitsmittel entwickelte. Mit 10 Jahren fing Lukas an, sich das Programmieren
                                    beizubringen. Angefangen hat es mit PAWN, einer Sprache für den Multiplayer von GTA
                                    San Andreas (SAMP). Es folgten die Sprachen AutoHotKey,PHP,HTML,CSS,Javascript,C#
                                    und C++ sowie die Kenntnisse zur Serveradministration Linux/Windows.
                                </p>
                                <p>
                                    Nach der abgeschlossenen Ausbildung zum Fachinformatiker Anwendungsentwicklung
                                    stellte seine Ausbildungsstätte, die S-IMK GmbH mit Sitz in Elsdorf Heppendorf, ihn
                                    als Anwendungsentwickler Web-Anwendungen im Home Office ein.
                                </p>
                            </div>
                            <div class="bts">
                                <!--<a href="#" class="btn btn_animated"><span class="circle">Download CV</span></a>-->
                                <!--<a href="#" class="btn extra contact-btn btn_animated"><span class="circle">Contact Me</span></a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Section About -->

            <!-- Section Skills -->
            <!--<div class="section skills" id="skills-section">
                <div class="title">Skills</div>
                <div class="row">
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="i_title">
                                <div class="icon"><i class="icon fa fa-gear"></i></div>
                                <div class="name">Professional</div>
                            </div>
                            <div class="skills">
                                <ul>
                                    <li>
                                        <div class="name">Website Design</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:10%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Website Development</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:100%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Database Design</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:100%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Application Development</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:59%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Software Engineering</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:99%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Project Management</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:73%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Linux Server Administration</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:95%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="i_title">
                                <div class="icon"><i class="icon fa fa-user"></i></div>
                                <div class="name">Personal</div>
                            </div>
                            <div class="skills">
                                <ul>
                                    <li>
                                        <div class="name">Communication</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:78%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="name">Teamworking</div>
                                        <div class="progress">
                                            <div class="percentage" style="width:85%;">
                                                <span class="percent"><i class="icon fa fa-check"></i></span>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Experience -->
            <div class="section experience" id="experience-section">
                <div class="title">
                    Arbeitsstationen
                    <span class="circle"><i class="icon fa fa-briefcase"></i></span>
                </div>
                <div class="cd-timeline">
                    <div class="cd-timeline-block animated">
                        <div class="cd-timeline-point">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <div class="cd-timeline-content">
                            <div class="content-box">
                                <div class="date">2017-heute</div>
                                <div class="name">S-IMK GmbH</div>
                                <div class="category">Anwendungsentwickler Web-Anwendungen (Home Office)</div>


                                <ul>
                                    <li>Planung und Entwicklung von Endkundenwebseiten, Sparkassenwebseiten und Restful API's für das Finanzwesen*</li>
                                    <li>Planung und Entwicklung von Mobile Apps**</li>
                                </ul>

                                <p style="font-size:12px; padding-top:10px;">* auf Basis von Laravel, HTML und Javascript <br />** mit Hilfe des Ionic Frameworks</p>

                            </div>
                        </div>
                    </div>
                    <div class="cd-timeline-block animated">
                        <div class="cd-timeline-point">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <div class="cd-timeline-content">
                            <div class="content-box">
                                <div class="date">2014-2017</div>
                                <div class="name">S-IMK GmbH</div>
                                <div class="category">Auszubildender Fachinformatiker Anwendungsentwicklung</div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <span class="circle"><i class="icon fa fa-university"></i></span>
                    </div>
                </div>
            </div>
            <!-- Section Education -->
            <div class="section education" id="education-section">
                <div class="title">
                    Schulbildung
                    <span class="circle"><i class="icon fa fa-university"></i></span>
                </div>
                <div class="cd-timeline">
                    <div class="cd-timeline-block animated">
                        <div class="cd-timeline-point">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <div class="cd-timeline-content">
                            <div class="content-box">
                                <div class="date">2014-2017</div>
                                <div class="name">Georg Simon Ohm Berufskolleg</div>
                                <div class="category">Köln</div>
                                <p>
                                    Ausbildung zum Fachinformatiker Anwendungsentwicklung
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="cd-timeline-block animated">
                        <div class="cd-timeline-point">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <div class="cd-timeline-content">
                            <div class="content-box">
                                <div class="date">2007-2014</div>
                                <div class="name">Realschule Bedburg</div>
                                <div class="category">Bedburg</div>
                            </div>
                        </div>
                    </div>
                    <div class="cd-timeline-block animated">
                        <div class="cd-timeline-point">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <div class="cd-timeline-content">
                            <div class="content-box">
                                <div class="date">2006-2007</div>
                                <div class="name">Geschwister-Stern-Schule-Kirchherten</div>
                                <div class="category">Bedburg</div>
                            </div>
                        </div>
                    </div>
                    <div class="cd-timeline-block animated">
                        <div class="cd-timeline-point">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <div class="cd-timeline-content">
                            <div class="content-box">
                                <div class="date">2002-2006</div>
                                <div class="name">Martinusschule Kaster</div>
                                <div class="category">Bedburg</div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <span class="circle"><i class="icon fa fa-child"></i></span>
                    </div>
                </div>


            </div>
            <!--<div class="section service" id="service-section">
                <div class="title">Angebote</div>
                <div class="row">
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="i_title">
                                <div class="icon"><i class="icon fa fa-chrome"></i></div>
                                <div class="name">Webseiten Entwicklung</div>
                            </div>
                            <p>
                                Ich entwickle Ihre Webseite, nach Ihren Vorgaben in den Sprachen PHP (mit dem Framework
                                Laravel), HTML 5, CSS 3 (mit Bootstrap) und Javascript (JQuery/VueJS/AngularJS). Vor
                                Start der Entwicklung bespreche ich mit Ihnen ihre Wünsche und bringe gerne eigene Ideen
                                mit.
                            </p>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="i_title">
                                <div class="icon pull-left"><i class="icon fa fa-android"></i></div>

                                <div class="icon pull-right"><i class="icon fa fa-apple"></i></div>
                                <div class="clear"></div>
                                <div class="name">App Entwicklung</div>
                            </div>
                            <p>
                                Sie möchten für Ihr Produkt eine schicke App haben? Die sowohl auf Android wie auf iOS
                                Geräten funktioniert? Die Entwicklung einer solchen App, ist mit Hilfe des Javascript
                                Frameworks ionic plattformunabhänig und schnell machbar.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="i_title">
                                <div class="icon"><i class="icon fa fa-server"></i></div>
                                <div class="name">Server Hosting</div>
                            </div>
                            <p>
                               Sie benötigen einen Platz für Ihre Webseite oder aber einen ganzen Server nur für sich? Ich biete Ihnen auf Ihren Bedarf zugeschnittene Systeme an, wahlweise übernehme ich auch die vollständige Wartung und Pflege für Sie!
                            </p>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="i_title">
                                <div class="icon"><i class="icon fa fa-gears"></i></div>
                                <div class="name">Projekt Managment</div>
                            </div>
                            <p>
                               Sie haben eine Idee, aber wissen nicht wie Sie es umsetzen sollen? Ich biete Ihnen meine Tätigkeit an! Durch meine Ausbildung als Fachinformatiker Anwendungsentwicklung, wurde ich schulisch als Projekt Manager ausgebildet und kann Ihnen so helfen Ihr Projekt zum Erfolg zu bringen!
                            </p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Projekts -->
            <div class="section blog" id="projects-section">
                <div class="title">Projekte und Angebote</div>
                <div class="row">
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="blog_item animated">
                            <div class="image">
                                <a href="https://lk-hosting.de"><img src="images/lk_hosting_mit_background.jpg" alt=""/></a>
                            </div>
                            <div class="content-box">
                                <div class="i_title">
                                    <div class="icon"><i class="icon fa fa-server"></i></div>
                                </div>
                                <a href="https://lk-hosting.de" class="name">LK-Hosting</a>
                                <p>
                                    Hosting von Webseiten und Servern zu günstigen Preisen mit 24/7 Support.
                                </p>
                                <a href="https://lk-hosting.de" class="btn btn_animated"><span class="circle">zu LK-Hosting.de</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="blog_item animated">
                            <div class="image">
                                <a href="https://lk-development.de"><img src="images/lk_development_mit_background.jpg" alt=""/></a>
                            </div>
                            <div class="content-box">
                                <div class="i_title">
                                    <div class="icon"><i class="icon fa fa-chrome"></i></div>
                                </div>
                                <a href="https://lk-development.de" class="name">LK-Development</a>
                                <p>
                                    Preiswerte Entwicklung von Anwendungen und Webseiten nach Ihrem Konzept.
                                </p>
                                <a href="https://lk-development.de" class="btn btn_animated"><span class="circle">zu LK-Development.de</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Section Contacts -->
            <div class="section contacts" id="contact-section">
                <div class="title">Kontakt</div>
                <div class="row">
                    <div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <div class="info-list">
                                <ul>
                                    <li><strong><span>Adresse:</span></strong> Sommerreithweg 4 94579 Zenting</li>
                                    <li><strong><span>E-Mail:</span></strong>
                                        <a href="mailto:{{ trans('frontpage.mailaddress') }}">{{ trans('frontpage.mailaddress') }}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="map" id="map"></div>
                        </div>
                    </div>
                    <!--<div class="col col-m-12 col-t-6 col-d-6">
                        <div class="content-box animated">
                            <h4>Write a message:</h4>
                            <div class="contact-form">
                                <form id="cform" method="post">
                                    <div class="group-val">
                                        <input type="text" name="name" placeholder="Your Name"/>
                                    </div>
                                    <div class="group-val">
                                        <input type="text" name="email" placeholder="Your Email"/>
                                    </div>
                                    <div class="group-val">
                                        <input type="text" name="subject" placeholder="Subject"/>
                                    </div>
                                    <div class="group-val ct-gr">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>
                                    <a href="#" class="btn btn_animated" onclick="$('#cform').submit(); return false;"><span class="circle">Send Message</span></a>
                                </form>
                                <div class="alert-success">
                                    <p>Thanks, your message is sent successfully. We will contact you shortly!</p>
                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="copy">© Lukas Kämmerling {{ date('Y') }} Design by mCard</div>
        </footer>

    </div>

</div>

<!-- jQuery Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/magnific-popup.js"></script>
<script src="js/masonry.pkgd.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/masonry-filter.js"></script>
<script src="js/scrollreveal.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="https://use.fontawesome.com/8086f651e0.js"></script>
<!-- Google map api -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyB9P71Jc1Kv6ioFQSdxp9mlbovDcEKqif0"></script>

<!-- Main Scripts -->
<script src="js/main.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-73424522-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-73424522-1');
</script>
</body>
</html>