let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/assets/js', 'public/js');
mix.copyDirectory('resources/assets/images', 'public/images');
mix.less('resources/assets/less/app.less', 'public/css');
mix.disableNotifications();