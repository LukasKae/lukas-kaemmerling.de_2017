<?php

return [
    'age' => 'Alter',
    'years' => 'Jahre',
    'job' => 'Job',
    'jobtitle' => 'Anwendungsentwickler Web-Anwendungen',
    'citizienship' => 'Staatsangehörigkeit',
    'citizienshipland' => 'Deutsch',
    'address' => 'Adresse',
    'phone' => 'Telefon',
    'mail' => 'E-Mail',
    'mailaddress' => 'kontakt@lukas-kaemmerling.de',
];